#!/usr/bin/env python2


def read_conf(path):
  c = None
  with open(path, 'r') as f:
    c = f.read()
  return c


def tokenize(text):
  in_quotes, in_comment = False, False
  t = ''
  for l in text:
    if in_quotes:
      if l != '"':
        t += l
      else:
        in_quotes = False
        yield ('QUOTED', t)
        t = ''
    elif in_comment:
      if l == '\n':
        in_comment = False
    elif l == '"':
      in_quotes = True
      if t:
        yield ('TEXT', t)
        t = ''
    elif l == '#':
      if t:
        yield ('TEXT', t)
        t = ''
      in_comment = True
    elif l in (' ', '\n'):
      if t:
        yield ('TEXT', t)
        t = ''
    elif l in ('{', '}'):
      if t:
        yield('TEXT', t)
        t = ''
      yield ('BRACE', l)
    else:
      t += l


def parse(text):
  tree = {}
  current_tree, parent_trees, last_key = tree, [], None
  for tok_type, tok_value in tokenize(text):
    if tok_type == 'BRACE' and tok_value == '}':
      if parent_trees == []:
        raise Exception('Unmatched {}')
      current_tree = parent_trees.pop()
      last_key = None
    elif not last_key:
      current_tree[tok_value] = None
      last_key = tok_value
    else:
      if tok_type in ('TEXT', 'QUOTED'):
        current_tree[last_key] = tok_value
        last_key = None
      elif tok_type == 'BRACE':
        if tok_value == '{':
          current_tree[last_key] = {}
          parent_trees.append(current_tree)
          current_tree = current_tree[last_key]
          last_key = None
        elif tok_value == '}':
          if parent_trees == []:
            raise Exception('Unmatched {}')
          current_tree = parent_trees.pop()
          last_key = None
  return tree


import json
print(json.dumps(parse(read_conf('example.conf')), indent=2))
